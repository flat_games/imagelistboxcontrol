﻿using System;
using System.Drawing;
using System.Windows.Forms;

using ImageListbox;


namespace TestingApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < 7; ++i) {
                imageListbox.AddItem(new c_ImageListboxItem(text: "test_text"));
            }
        }
    }
}
