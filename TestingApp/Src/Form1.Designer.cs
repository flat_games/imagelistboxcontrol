﻿namespace TestingApp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.imageListbox = new ImageListbox.c_ImageListbox();
            this.SuspendLayout();
            // 
            // imageListbox
            // 
            this.imageListbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.imageListbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageListbox.Location = new System.Drawing.Point(13, 13);
            this.imageListbox.MinimumSize = new System.Drawing.Size(100, 100);
            this.imageListbox.Name = "imageListbox";
            this.imageListbox.pr_DefaultItemColor = System.Drawing.SystemColors.Control;
            this.imageListbox.pr_SelectedItemColor = System.Drawing.Color.CornflowerBlue;
            this.imageListbox.pr_ushImageSize = ((ushort)(38));
            this.imageListbox.Size = new System.Drawing.Size(216, 536);
            this.imageListbox.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 561);
            this.Controls.Add(this.imageListbox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private ImageListbox.c_ImageListbox imageListbox;
    }
}

