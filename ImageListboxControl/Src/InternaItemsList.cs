﻿/*
 * классы, используемый в реализации и работе контрола
 */
using global::System;
using System.Collections;
using System.Collections.Generic;

using SCM = System.ComponentModel;


namespace ImageListbox
{
    public partial class c_ImageListbox
    {
        //тип делегата, используемый в событии c_ImageListboxItemsList::ev_ListChanged:
        /* lengthDelta - изменение длинны списка; items - массив добавленных/удаленных элементов (если удалили все, то будет null) */
        private delegate void dl_listLengthChanged(int lengthDelta, params c_ImageListboxItem[] items);
        //used in event ev_ListElementChanged:
        private delegate void dl_listElementChanged(int index, c_ImageListboxItem changedItem);


        //Коллекция элементов VisualListbox`a с эвентом, позволяющим отслеживать момент добавления и удаления:
        /* я не совсем точно соблюдал требования спецификации IList, но для моих целей этого и не требуется */
        [type: SCM::ListBindable(false), Serializable]
        private class c_itemsList : IList, IList<c_ImageListboxItem>
        {
            private List<c_ImageListboxItem> _itemsList;
            private dl_listLengthChanged __dlListLengthChangedProc = null;
            private dl_listElementChanged __dlListElementChangedProc = null;


            //срабатывает, когда изменяется длина списка:
            public event dl_listLengthChanged ev_ListChanged
            {
                add { __dlListLengthChangedProc = value ?? __dlListLengthChangedProc; } //rewrite with new value, not add!
                remove { __dlListLengthChangedProc -= value; }
            }
            //срабатывает, когда изменяется (перезаписывается через мутатор) какой-либо конкретный элемент:
            public event dl_listElementChanged ev_ListElementChanged
            {
                add { __dlListElementChangedProc = value ?? __dlListElementChangedProc; }
                remove { __dlListElementChangedProc -= value; }
            }


            object IList.this[int index]
            {
                get => _itemsList[index];
                set {
                    if (value is c_ImageListboxItem item) {
                        _itemsList[index] = item;
                        __dlListElementChangedProc?.Invoke(index, item);
                    }
                    else throw getTypeOrNullArgumentException(value, nameof(value));
                }
            }
            public c_ImageListboxItem this[int index]
            {
                get => _itemsList[index];
                set {
                    _itemsList[index] = value;
                    __dlListElementChangedProc?.Invoke(index, value);
                }
            }


            /*
             * IList:
             */
            public bool IsReadOnly => false;
            public bool IsFixedSize => false;
            public int Count => _itemsList.Count;
            public object SyncRoot => (_itemsList as IList).SyncRoot;
            public bool IsSynchronized => (_itemsList as IList).IsSynchronized;


            /*
             * IList:
             */
            int IList.Add(object value)
            {
                var item = (value as c_ImageListboxItem) ?? new c_ImageListboxItem();
                _itemsList.Add(item);
                __dlListLengthChangedProc?.Invoke(+1, item);
                return _itemsList.Count - 1;    //позиция вставки
            }
            void IList.Clear()
            {
                int ln = _itemsList.Count;
                _itemsList.Clear();
                __dlListLengthChangedProc?.Invoke(-ln, null);
            }
            bool IList.Contains(object value)
            {
                var item = (value as c_ImageListboxItem) ?? throw getTypeOrNullArgumentException(value, nameof(value));
                return _itemsList.Contains(item);
            }
            void ICollection.CopyTo(Array array, int index)
            {
                if (array is c_ImageListboxItem[] items_array) {
                    _itemsList.CopyTo(items_array, index);
                }
                else throw getTypeOrNullArgumentException(array, nameof(array), is_array: true);
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return _itemsList.GetEnumerator();
            }
            int IList.IndexOf(object value)
            {
                if (value is c_ImageListboxItem item) {
                    return _itemsList.IndexOf(item);
                }
                else throw getTypeOrNullArgumentException(value, nameof(value));
            }
            void IList.Insert(int index, object value)
            {
                if (value is c_ImageListboxItem item) {
                    _itemsList.Insert(index, item);
                    __dlListLengthChangedProc?.Invoke(+1, item);
                }
                else throw getTypeOrNullArgumentException(value, nameof(value));
            }
            void IList.Remove(object value)
            {
                if (value is c_ImageListboxItem item) {
                    //we should not call __dlListLengthChangedProc, if value not contais in list
                    if (_itemsList.Contains(item)) {
                        _itemsList.Remove(item);
                        __dlListLengthChangedProc?.Invoke(-1, item);
                    }
                }
                else throw getTypeOrNullArgumentException(value, nameof(value));
            }
            void IList.RemoveAt(int index)
            {
                var item = _itemsList[index];
                _itemsList.RemoveAt(index);
                __dlListLengthChangedProc?.Invoke(-1, item);
            }

            /*
             * IList<c_ImageListboxItem>:
             */
            public int IndexOf(c_ImageListboxItem item)
            {
                return _itemsList.IndexOf(item);
            }
            public void Insert(int index, c_ImageListboxItem item)
            {
                //тайпкаст для наглядности (сдесь и далее)
                (( IList )this).Insert(index, item as object);
            }
            public void Add(c_ImageListboxItem item)
            {
                (( IList )this).Add(item as object);
            }
            public void Clear()
            {
                (( IList )this).Clear();
            }
            public bool Contains(c_ImageListboxItem item)
            {
                return (( IList )this).Contains(item);
            }
            public void CopyTo(c_ImageListboxItem[] array, int arrayIndex)
            {
                (( IList )this).CopyTo(array, arrayIndex);
            }
            public bool Remove(c_ImageListboxItem item)
            {
                try {
                    (( IList )this).Remove(item as object);
                }
                catch {
                    return false;
                }
                return true;
            }
            public void RemoveAt(int index)
            {
                (( IList )this).RemoveAt(index);
            }

            IEnumerator<c_ImageListboxItem> IEnumerable<c_ImageListboxItem>.GetEnumerator()
            {
                return _itemsList.GetEnumerator();
            }


            //возвращает сообщение об ошибке несоответствия типов/пустого аргумента:
            /*
             * arg - аргумент, с которым связано исключение., если = null, то функция вернет тип исключения ArgumentNullException, иначе простой Exception;
             * is_array - флаг, указывающи на то, должен ли arg быть массивом cVLBI_t, или же это одиночный элемент (того же типа);
             * arg_name - имя аргумента, используемое при формировании ArgumentNullException
             */
            private Exception getTypeOrNullArgumentException(object arg, string arg_name, bool is_array = false)
            {
                if (arg == null) {
                    return new ArgumentNullException(arg_name, "Argument has NULL value");
                }
                else {
                    string required_type_name;

                    if (is_array)
                        required_type_name = typeof(c_ImageListboxItem[]).Name;
                    else
                        required_type_name = typeof(c_ImageListboxItem).Name;

                    return new Exception($"Wrong type! Value has {arg.GetType().Name} type. Must be {required_type_name}");
                }
            }



            //ctor:
            public c_itemsList()
            {
                this._itemsList = new List<c_ImageListboxItem>();
            }
        } //c_itemsList
    } //c_ImageListbox
}
