﻿using global::System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;


namespace ImageListbox
{
    //c_ImageListboxItem представляет один элемент листбокса (контрол не предназначен для самостоятельного использования. он является составным элементом VisualListbox`a):
    /*
     * картинка, отображаемая в контроле всегда квадратная. её размер = высоте контрола
     */
    [type: Serializable, DesignTimeVisible(false)]
    public class c_ImageListboxItem : Panel
    {
        private const int cn_nMinimumLabelWidth = 40;   //минимально допустимая ширина лейбла в пикселях. если лейбл будет меньше, он будет автоматически скрыт
        /* static: */
        private static Image st_defaultImage;           //изображение, которое будет выставлено поумолчанию, если пользователь не задаст своего

        private PictureBox pictureBox;
        private Label label;
        private Color _itemColor;       //цвет данного контрола (ставится в фон base и всех дочерних контролов)
#pragma warning disable IDE0032
        private en_ImageListboxItemShowMode _showMode = en_ImageListboxItemShowMode.eImageAndText;
        private object _oTag = null;    //пользовательские данные
#pragma warning restore IDE0032


        [property:
         DisplayName("Image"),
         Description("Отображаемое изображение (если не задано, то будет установлено стандартное изображение)"),
         Category("ImageListbox")]
        public Image pr_Image
        {
            get => pictureBox.BackgroundImage;
            set {
                pictureBox.BackgroundImage = value;
            }
        }

        [property:
         DisplayName("Text"),
         Description("Отображаемый тест"),
         Category("ImageListbox")]
        public string pr_sText
        {
            get => label.Text;
            set {
                label.Text = value;
            }
        }

        [property:
         DisplayName("Tag"),
         Description("Пользовательские данные, привязанные к элементу"),
         Category("ImageListbox")]
        public object pr_oTag
        {
            get => _oTag;
            set { _oTag = value; }
        }

        [property:
         DisplayName("AutoLabelVisibilityControl"),
         Description("Флаг автоматического переключения видимости текста при изменении размеров контрола (если текст не влезает, он будет скрыт)"),
         Category("ImageListbox"),
         DefaultValue(true)]
        public bool pr_bAutoVisibilityControl { get; set; } = true;

        [property:
         DisplayName("ItemColor"),
         Description("Определяет цвет фона и цвет плашки с текстом"),
         Category("ImageListbox")]
        public Color pr_ItemColor
        {
            get => _itemColor;
            set {
                _itemColor = value;
                label.BackColor = value;
                pictureBox.BackColor = value;
                base.BackColor = value;
            }
        }

        [property: Browsable(false)]
        public en_ImageListboxItemShowMode pr_ShowMode
        {
            get => _showMode;
            set { SetShowMode(value); }
        }

        //позиция контрола в родительском элементе:
        [property: Browsable(false)]
        public int pr_nLocationX
        {
            get => base.Location.X;
            set { base.Location = new Point(value, Location.Y); }
        }

        [property: Browsable(false)]
        public int pr_nLocationY
        {
            get => base.Location.Y;
            set { base.Location = new Point(Location.X, value); }
        }

        //режим отображения в pictureBox:
        [property: Browsable(false)]
        public ImageLayout pr_ImageLayout
        {
            get => pictureBox.BackgroundImageLayout;
            set { pictureBox.BackgroundImageLayout = value; }
        }


        /* Public: */
        public void RestoreDefaultImage()
        {
            pictureBox.BackgroundImage = st_defaultImage;
        }
        public void SetSize(ushort width, ushort height)
        {
            if (height > width)
                throw new ArgumentException($@"{nameof(width)} must be >= {nameof(height)}");

            base.Size = new Size(width, height);

            updateInternalControls();
        }
        public void SetWidth(ushort width)
        {
            SetSize(width, ( ushort )base.Height);
        }
        public void SetHeight(ushort height)
        {
            SetSize(( ushort )base.Width, height);
        }
        public void SetShowMode(en_ImageListboxItemShowMode mode)
        {
            _showMode = mode;
            updateInternalControls();
        }


        /* Private: */
        //устанавливает позиции контролов, их размер и свойство Visible в соответствии с установленным _showMode:
        private void updateInternalControls()
        {
            int height = base.Height;
            int width = base.Width;
            int delta = width - height; //эту разницу можно интерпретировать, как оставшееся место (по ширене) после вычета размера изображения.

            switch (_showMode) {
                //в этом режиме изображение выставляется по центру контрола, а текстбокс исчезает:
                case en_ImageListboxItemShowMode.eImage:
                    label.Visible = false;
                    pictureBox.Visible = true;
                    pictureBox.Size = new Size(height, height); //высота контрола берется за мерку размера для пикчербокса
                    pictureBox.Location = new Point((width / 2 - height / 2), 0);  //позиция по центру = половина ширены (width / 2) минус половина размера изображения (height / 2)
                    break;

                //в этом режиме текстбокс занимает все пространство контрола, а изображение исчезает:
                case en_ImageListboxItemShowMode.eText:
                    pictureBox.Visible = false;
                    label.Visible = true;
                    label.Size = new Size(width, height);
                    label.Location = new Point(0, 0);
                    break;

                //этот режим срабатывает, если включена функция pr_bAutoVisibilityControl и размер размер текстбокса (который = delta) слишком мал (и _showMode = eImageAndText):
                case en_ImageListboxItemShowMode.eImageAndText when (pr_bAutoVisibilityControl && delta < cn_nMinimumLabelWidth):
                    goto case (en_ImageListboxItemShowMode.eImage); //скроет текст и расположет картинку по центру

                //режим совмещения текста и картинки в одном контроле:
                case en_ImageListboxItemShowMode.eImageAndText:
                    pictureBox.Visible = true;
                    label.Visible = true;
                    pictureBox.Size = new Size(height, height); //высота контрола берется за мерку размера для пикчербокса
                    label.Size = new Size(delta, height);       //в ширину лейбла записывается остаток от размера контрола за вычетом размера пикчербокса
                    pictureBox.Location = new Point(0, 0);
                    label.Location = new Point(height, 0);
                    break;
            }
        }

        //эти функции перенаправляют эвенты дочерних контролов (pictureBox и label) на базовый контрол:
        private void controls_mouseDown(object sender, MouseEventArgs e)
        {
            base.OnMouseDown(e);    //вызывет эвент MouseDown в данном контроле (оповестит Owner'a)
        }
        private void controls_mouseMove(object sender, MouseEventArgs e)
        {
            base.OnMouseMove(e);
        }
        private void controls_mouseUp(object sender, MouseEventArgs e)
        {
            base.OnMouseUp(e);
        }
        private void controls_mouseClick(object sender, MouseEventArgs e)
        {
            base.OnMouseClick(e);
        }
        private void controls_mouseDoubleClick(object sender, MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
        }


        //подписывает item на ряд эвентов контрола:
        private static void st_signItemOnEventsOfControl(c_ImageListboxItem item, Control control)
        {
            control.MouseDown += new MouseEventHandler(item.controls_mouseDown);
            control.MouseMove += new MouseEventHandler(item.controls_mouseMove);
            control.MouseUp += new MouseEventHandler(item.controls_mouseUp);
            control.MouseClick += new MouseEventHandler(item.controls_mouseClick);
            control.MouseDoubleClick += new MouseEventHandler(item.controls_mouseDoubleClick);
        }


        //ctor:
        public c_ImageListboxItem()
        {
            base.Size = new Size(250, 100);
            base.BorderStyle = BorderStyle.FixedSingle;
            base.Location = new Point(0, 0);

            this.pictureBox = new PictureBox();
            pictureBox.BackgroundImage = st_defaultImage;
            pictureBox.BorderStyle = BorderStyle.FixedSingle;
            base.Controls.Add(pictureBox);

            this.label = new Label();
            label.AutoEllipsis = true;
            label.AutoSize = false;
            label.Text = null;
            base.Controls.Add(label);

            //подписать перенаправляющие функции на соответствующие эвенты во всех содержащихся контролах:
            foreach (Control control in base.Controls) {
                st_signItemOnEventsOfControl(item: this, control);
            }

            this._itemColor = SystemColors.Control;  //цвет поумолчанию
            pr_ItemColor = _itemColor;  //обновит цвет во всех дочерних контролах

            SetSize(100, 50);
        }
        public c_ImageListboxItem(object tag)
            : this()
        {
            this._oTag = tag;
        }
        public c_ImageListboxItem(string text, object tag = null)
            : this(tag)
        {
            pr_sText = text;
        }
        public c_ImageListboxItem(Image img, object tag = null)
            : this(tag)
        {
            pr_Image = img;
        }
        public c_ImageListboxItem(Image img, string text, object tag = null)
            : this(tag)
        {
            pr_sText = text;
            pr_Image = img;
        }

        internal c_ImageListboxItem(int pos_x, int pos_y, Image img, string text, ushort control_wid, ushort control_hgt, object tag = null)
            : this(tag)
        {
            base.Location = new Point(pos_x, pos_y);
            pr_Image = img;
            pr_sText = text;
            SetSize(control_wid, control_hgt);
        }

        //static ctor (создаем st_defaultImage):
        static c_ImageListboxItem()
        {
            using (var gr = Graphics.FromImage(st_defaultImage = new Bitmap(50, 50))) {
                gr.Clear(Color.White);
                gr.DrawRectangle(Pens.Black, 2, 2, 45, 45);
                gr.DrawLine(Pens.Red, 4, 4, 45, 45);
                gr.DrawLine(Pens.Red, 4, 45, 45, 4);
            }
        }
    }

}