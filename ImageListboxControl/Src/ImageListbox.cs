﻿/*
 * Контрол ImageListbox, представляющий из себя список картинок и привязанного к ним текста
 */
using global::System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;


using SCM = System.ComponentModel;

// using cVLBI_t = ImageListbox.c_ImageListbox.c_VisualListboxItem;
// using cVLBIList_t = ImageListbox.c_ImageListbox.c_itemsList;


namespace ImageListbox
{
    //возможные варианты отображения элементов ImageListbox'a:
    public enum en_ImageListboxItemShowMode : byte
    {
        eImage          = 0b0000_0001,  //только изображение
        eText           = 0b0000_0010,  //только текст
        eImageAndText   = 0b0000_0011   //и текст и изображение
    }


    //режимы отображения вертикальной полосы прокрутки в ImageListbox'e:
    public enum en_ImageListboxScrollBarMode : byte
    {
        eHide = 0x00,   //всегда скрыт
        eShow = 0x01,   //всегда показан
        eAuto = 0x02    //автоматическое переключение видимости в зависимости от размеров контрола и количества элементов в списке
    }


    [SCM::DesignTimeVisible(true)]
    public partial class c_ImageListbox : UserControl
    {
        /* static: */
        private const int cn_nElementsVBias_px = 6;     //вертикальный отступ между элементами в пикселях
        //-----------------------------------------------------------------
        private c_itemsList _itemsList;
        private c_ImageListboxItem _selectedItem = null;    //ссылка на выделенный в данный момент элемент списка (будет null, если нет выбранных элементов)
        private en_ImageListboxItemShowMode _itemShowMode;
        private ImageLayout _itemImageLayout;           //это значение будет передано в свойство BackgroundImageLayout PictureBox`ов, создаваемых внутри элементов
        private bool _bItemTextAutoVisibility = true;   //флаг автопереключения видимости текста при изменении размеров контрола (плашка с текстом отключается, если она не влезает в элемент)
        private bool _bItemImageAutoSize = true;        //флаг автоустановки размера изображений по _fSizeProportion с учетом ширины базового контрола; при false, размер берется из _ushImageSize
        private float _fItemSizeProportion = 0.38f;     //отношение ширины изображения к ширене контрола (имеются в виду контролы-элементы списка)
        private ushort _ushItemImageSize;               //длина стороны изображения (все изображения квадратные width = height = side); применяется для ручной настройки размеров, если _bAutoSize = false
        private en_ImageListboxScrollBarMode _scrollBarMode;    //режим отображения скроллбара
        private Color _defaultItemColor;                //обычный цвет итемов
        private Color _selectedItemColor;               //цвет выделенных итемов
        private Action<c_ImageListboxItem> __dlSelectedItemChanged = null;


        [property:
         SCM::DisplayName("ItemsCollection"),
         SCM::Description("Список элементов контрола"),
         SCM::Category("ImageListbox")]
        public IList<c_ImageListboxItem> pr_ItemsList_ro => _itemsList;

        [property: SCM::Browsable(false)]
        public c_ImageListboxItem pr_SelectedItem_ro => _selectedItem;

        [property: SCM::Browsable(false)]
        public int pr_nSelectedIndex_ro
        {
            get {
                return _selectedItem != null ? _itemsList.IndexOf(_selectedItem) : -1;
            }
        }

        [property:
         SCM::DisplayName("ItemShowMode"),
         SCM::Description("Задает режим отображения элементов контрола: только текст || только изображение || и текст и изображение"),
         SCM::Category("ImageListbox"),
         SCM::DefaultValue(typeof(en_ImageListboxItemShowMode), "eImageAndText")]
        public en_ImageListboxItemShowMode pr_ItemShowMode
        {
            get => _itemShowMode;
            set {
                _itemShowMode = value;
                updateItemsShowMode();
            }
        }

        [property:
         SCM::DisplayName("ItemImagesLayout"),
         SCM::Description("Режим отображения картинок в списке"),
         SCM::Category("ImageListbox"), SCM::DefaultValue(typeof(ImageLayout), "Stretch")]
        public ImageLayout pr_ItemImageLayout
        {
            get => _itemImageLayout;
            set {
                _itemImageLayout = value;
                updateItemsImageLayout();
            }
        }

        [property:
         SCM::DisplayName("ItemTextAutoVisibility"),
         SCM::Description("Флаг автоматического переключения видимости текста при изменении размеров контрола (если текст не влезает, он будет скрыт)"),
         SCM::Category("ImageListbox"), SCM::DefaultValue(true)]
        public bool pr_bItemTextAutoVisibility
        {
            get => _bItemTextAutoVisibility;
            set {
                _bItemTextAutoVisibility = value;
                updateAutoVisibilityControl();
            }
        }

        [property:
         SCM::DisplayName("AutoSize"),
         SCM::Description("Флаг автоматической настройки размеров контролов. Если включен, то размер контролов будет подбираться по заданному SizeProportion, иначе будет использоваться ImageSize"),
         SCM::Category("ImageListbox"), SCM::DefaultValue(true)]
        public bool pr_bAutoSize
        {
            get => _bItemImageAutoSize;
            set {
                _bItemImageAutoSize = value;

                resetListPosAndConfigureScrollBar();    //выполнит сброс позиций контролов и их перенастройку
            }
        }

        [property:
         SCM::DisplayName("ItemSizeProportion"),
         SCM::Description("Отношение ширины изображения к общей ширине элементов (задействуется при AutoSize = true)"),
         SCM::Category("ImageListbox"), SCM::DefaultValue(0.38f/*как-бы золотое сечение...*/)]
        public float pr_fItemSizeProportion
        {
            get => _fItemSizeProportion;
            set {
                _fItemSizeProportion = value;

                //если значение _fSizeProportion используется:
                if (_bItemImageAutoSize)
                    resetListPosAndConfigureScrollBar();    //выполнит сброс позиций контролов и их перенастройку
            }
        }

        [property:
         SCM::DisplayName("ImageSize"),
         SCM::Description("Определяет размер изображений при AutoSize = false"),
         SCM::Category("ImageListbox")]
        public ushort pr_ushImageSize
        {
            get => _ushItemImageSize;
            set {
                _ushItemImageSize = value;

                //если значение _ushImageSize используется:
                if (!_bItemImageAutoSize)
                    resetListPosAndConfigureScrollBar();    //выполнит сброс позиций контролов и их перенастройку
            }
        }

        [property:
         SCM::DisplayName("ScrollBarMode"),
         SCM::Description("Задает режим отображения сколлбара"),
         SCM::Category("ImageListbox"), SCM::DefaultValue(typeof(en_ImageListboxScrollBarMode), "eAuto")]
        public en_ImageListboxScrollBarMode pr_ScrollBarMode
        {
            get => _scrollBarMode;
            set {
                _scrollBarMode = value;

                //задать видимость скроллбара:
                switch (value) {
                    case (en_ImageListboxScrollBarMode.eHide): VScrollBar.Visible = false; break;
                    case (en_ImageListboxScrollBarMode.eShow): VScrollBar.Visible = true; break;
                    case (en_ImageListboxScrollBarMode.eAuto): VScrollBar.Visible = isVScrollBarNeeded(); break;
                }
                resetListPosAndConfigureScrollBar();
            }
        }

        [property:
         SCM::DisplayName("DefaultItemColor"),
         SCM::Description("Цвет невыделенных элементов"),
         SCM::Category("ImageListbox")]
        public Color pr_DefaultItemColor
        {
            get => _defaultItemColor;
            set {
                _defaultItemColor = value;
                updateItemColor();
            }
        }

        [property:
         SCM::DisplayName("SelectedItemColor"),
         SCM::Description("Цвет выделенных элементов"),
         SCM::Category("ImageListbox")]
        public Color pr_SelectedItemColor
        {
            get => _selectedItemColor;
            set {
                _selectedItemColor = value;

                if (_selectedItem != null)
                    _selectedItem.pr_ItemColor = value;
            }
        }

        [property: SCM::Browsable(false)]
        public int pr_nItemsCount_ro
        {
            get => _itemsList.Count;
        }


        public c_ImageListboxItem this[int index]
        {
            get => _itemsList[index];
            set { _itemsList[index] = value; }
        }


        //срабатывает при изменении _selectedItem:
        [event:
         SCM::DisplayName("SelectedItemChanged"),
         SCM::Description("Fires when user changes the selection.")]
        public event Action<c_ImageListboxItem> ev_SelectedItemChanged
        {
            add { __dlSelectedItemChanged += value; }
            remove { __dlSelectedItemChanged -= value; }
        }


        //добавить элемент в список (это же можно сделать вручную, получив список через свойство pr_ItemsList_ro):
        public void AddItem(Image img, string text, object tag = null) { _itemsList.Add(new c_ImageListboxItem(img, text, tag)); }
        public void AddItem(string text, object tag = null) { _itemsList.Add(new c_ImageListboxItem(text, tag)); }
        public void AddItem(Image img, object tag = null) { _itemsList.Add(new c_ImageListboxItem(img, tag)); }
        public void AddItem(c_ImageListboxItem item) { _itemsList.Add(item); }

        public void InsertItem(int index, Image image, string text, object tag = null) { _itemsList.Insert(index, new c_ImageListboxItem(image, text, tag)); }
        public void InsertItem(int index, string text, object tag = null) { _itemsList.Insert(index, new c_ImageListboxItem(text, tag)); }
        public void InsertItem(int index, Image image, object tag = null) { _itemsList.Insert(index, new c_ImageListboxItem(image, tag)); }
        public void InsertItem(int index, c_ImageListboxItem item) { _itemsList.Insert(index, item); }

        //удалить элемент из списка:
        public void Remove(c_ImageListboxItem item) { _itemsList.Remove(item); }
        public void RemoveAt(int index) { _itemsList.RemoveAt(index); }
        public void RemoveAll() { _itemsList.Clear(); }

        //снять выделение с сущности _selectedItem:
        public void Deselect(bool callEventOfSelectedItemChanged = true)
        {
            if (_selectedItem != null)
                _selectedItem.pr_ItemColor = _defaultItemColor;

            _selectedItem = null;

            if (callEventOfSelectedItemChanged) {
                __dlSelectedItemChanged?.Invoke(null);
            }
        }
        //передать выделение конкретной сущности:
        public void Select(c_ImageListboxItem item, bool callEventOfSelectedItemChanged = true)
        {
            if (item == null) {
                Deselect(callEventOfSelectedItemChanged);
                return;
            }
            if (!_itemsList.Contains(item))
                throw new ArgumentException("Argument must be in items list", nameof(item));

            //снять старое выделение:
            Deselect(callEventOfSelectedItemChanged);

            //сделать новое выделение:
            _selectedItem = item;

            //обновить цвет нового выделения:
            _selectedItem.pr_ItemColor = _selectedItemColor;

            if (callEventOfSelectedItemChanged) {
                __dlSelectedItemChanged?.Invoke(item);
            }
        }
        public void Select(int index, bool callEventOfSelectedItemChanged = true)
        {
            if (index == -1) {
                Deselect(callEventOfSelectedItemChanged);
                return;
            }
            Select(_itemsList[index], callEventOfSelectedItemChanged);
        }


        /*
         * Event handlers:
         */
        //подписан на эвент изменения размера данного контрола. Меняет размеры дочерних контролов:
        private void controlSizeChanged(object sender, EventArgs e)
        {
            //сместить все элементы в 0 и поправить их позиции, перенастроить скроллбар:
            resetListPosAndConfigureScrollBar();
        }
        //подписан на скролл вертикального скроллбара:
        private void scrollBarScroll(object sender, ScrollEventArgs e)
        {
            changeControlsYPos(-VScrollBar.Value);
        }
        //подписан на эвент изменения списка _itemsList:
        private void itemsListChanged(int delta_length, params c_ImageListboxItem[] items)
        {
            //при любых изменениях списка снять выделение:
            Deselect();

            //если очистили список:
            if (items == null) {
                base.Controls.Clear();          //убрать все контролы (элементы списка и скроллбар)
                base.Controls.Add(VScrollBar);  //вернуть скроллбар на место
            }
            //если добавили элементы:
            else if (delta_length > 0) {
                //добавляем все контролы к нам на базовый контрол, попутно меняя их свойства:
                foreach (var it in items) {
                    base.Controls.Add(it);
                    st_configureItem(it, this);
                }
            }
            //если удалили элементы:
            else {
                //удаляем элементы с базовго контрола:
                foreach (var it in items) {
                    base.Controls.Remove(it);
                }
            }

            //сместить все элементы в 0 и поправить их позиции, перенастроить скроллбар:
            resetListPosAndConfigureScrollBar();
        }
        //подписан на эвент MouseDown во всех дочерних контролах:
        private void items_mouseDown(object sender, MouseEventArgs e)
        {
            var it = sender as c_ImageListboxItem;

            Select(it); //выделить итем по которому кликнули мышкой
        }


        //устанавливает режим отображения внутренних элементов по _showMode для всех контролов:
        private void updateItemsShowMode()
        {
            foreach (c_ImageListboxItem item in _itemsList) {
                item.SetShowMode(_itemShowMode);
            }
        }

        //устанавливает режим отображения изображения в пикчербоке по _imageLayout для всех контролов:
        private void updateItemsImageLayout()
        {
            foreach (c_ImageListboxItem item in _itemsList) {
                item.pr_ImageLayout = _itemImageLayout;
            }
        }

        //меняет размеры всех хранимых контролов, чтобы они соответствовали ширене данного контрола-owner`a:
        private void updateControlsSize()
        {
            //ширина контролов-элементов списка (зависит от отображения скроллбара):
            ushort item_width = ( ushort )(VScrollBar.Visible ? base.Width - VScrollBar.Width : base.Width);

            foreach (c_ImageListboxItem item in _itemsList)
            {
                //если размер изображения должен подбираться автоматически по пропорции:
                if (_bItemImageAutoSize) {
                    ushort img_sz = ( ushort )(item_width * _fItemSizeProportion);
                    item.SetSize(item_width, img_sz);
                }
                //если размер изображения задан:
                else {
                    try {
                        //может произойти ошибка, если высота > ширены
                        item.SetSize(item_width, _ushItemImageSize);
                    }
                    catch {
                        _ushItemImageSize = Convert.ToUInt16(base.MinimumSize.Width * _fItemSizeProportion);
                        item.SetSize(item_width, _ushItemImageSize);
                    }
                }
            }

        }

        //меняет значение свойства pr_bAutoVisibilityControl для всех элементов:
        private void updateAutoVisibilityControl()
        {
            foreach (c_ImageListboxItem item in _itemsList) {
                item.pr_bAutoVisibilityControl = _bItemTextAutoVisibility;
            }
        }

        //меняет значение свойства pr_ItemColor в итемах на _defaultItemColor:
        private void updateItemColor()
        {
            foreach (var item in _itemsList)
                item.pr_ItemColor = _defaultItemColor;
        }

        //меняет позицию X всех контролов (при этом не меняя их ширины!):
        private void changeControlsXPos(int x)
        {
            foreach (c_ImageListboxItem item in _itemsList) {
                item.pr_nLocationX = x;
            }
        }

        //меняет позицию элементов по Y (через эту функцию осуществляется скролл и выравнивание по Y):
        private void changeControlsYPos(int first_y /*позиция первого эл-та. остальные идут под ним*/)
        {
            if (_itemsList.Count == 0) return;  //если нет элементов, функция не делает ничего

            int pos_y = first_y;    //позиция размещения текущего элемента в цикле по всем элементам

            foreach (c_ImageListboxItem item in _itemsList) {
                item.pr_nLocationY = pos_y;
                pos_y += (item.Height + cn_nElementsVBias_px);   //смещение к следующему элементу = высота текущего + заданное расстояние
            }
        }


        //вычисляет суммарную высоту полосы контролов в пикселях:
        private int calcTotalHeight_px()
        {
            int hgt = 0;

            foreach (var item in _itemsList as IList<c_ImageListboxItem>) {
                hgt += item.Height;
            }

            //учитываем вертикаьные интервалы между элементами:
            if(_itemsList.Count > 0)
             hgt += (_itemsList.Count - 1) * cn_nElementsVBias_px;

            return hgt;
        }
        //возвращает true, если элементы по высоте не умещаются в данный контрол (это значит, что следует включать скроллбар):
        private bool isVScrollBarNeeded()
        {
            return base.Height < calcTotalHeight_px();
        }
        //задает параметры VScrollBar в соответствии с количеством элементов (но не меняет настройки видимости скроллбара):
        private void configureVScrollBar()
        {
            int hgt = calcTotalHeight_px();

            if (hgt <= base.Height || _itemsList.Count == 0) return;

            VScrollBar.Value = 0;   //обнуляем параметры (т.к. после изменения нстроек старые значения все равно потеряют смысл)
            VScrollBar.Maximum = (hgt - base.Height) + _itemsList[0].Height + cn_nElementsVBias_px;

            VScrollBar.LargeChange = hgt / _itemsList.Count;    //_itemsList.Count != 0 at this point (see condition above)

            VScrollBar.SmallChange = VScrollBar.LargeChange;
        }
        //перенастраивает скроллбар, выравнивает все контролы по левой стороне (с учетом наличия скроллбара), откатывает их Y-позицию в 0:
        private void resetListPosAndConfigureScrollBar()
        {
            //если требуется автонастройка видимости скроллбара:
            if (_scrollBarMode == en_ImageListboxScrollBarMode.eAuto) {
                VScrollBar.Visible = isVScrollBarNeeded();
            }

            changeControlsXPos(VScrollBar.Visible ? VScrollBar.Width : 0);  //изменить горизонтальное размещение элементов с учетом видимости скроллбара

            updateControlsSize();   //изменить размеры контролов после перемещения по Х

            changeControlsYPos(0);  //сместить все контролы в начало и выровнять по Y
            configureVScrollBar();  //перенастроить скроллбар
        }


        //задает в указанном элементе все параметры, определяемые базовым контролом:
        private static void st_configureItem(c_ImageListboxItem item, c_ImageListbox owner)
        {
            item.pr_bAutoVisibilityControl = owner.pr_bItemTextAutoVisibility;
            item.pr_nLocationX = (owner.VScrollBar.Visible ? owner.VScrollBar.Width : 0);
            item.SetShowMode(owner.pr_ItemShowMode);
            item.pr_ImageLayout = owner.pr_ItemImageLayout;
            item.pr_ItemColor = owner.pr_DefaultItemColor;
            item.MouseDown += new MouseEventHandler(owner.items_mouseDown);

            ushort item_wid = ( ushort )(owner.Width - item.pr_nLocationX);

            if (owner.pr_bAutoSize) {
                ushort img_sz = ( ushort )(owner._fItemSizeProportion * item_wid);
                item.SetSize(item_wid, img_sz);
            }
            else
                item.SetSize(( ushort )item_wid, owner._ushItemImageSize);
        }


        //ctor:
        public c_ImageListbox()
        {
            InitializeComponent();  //auto generated

            this._itemsList = new c_itemsList();
            this._itemShowMode = en_ImageListboxItemShowMode.eImageAndText;
            this._itemImageLayout = ImageLayout.Stretch;
            this._ushItemImageSize = Convert.ToUInt16(base.MinimumSize.Width * _fItemSizeProportion);
            this._scrollBarMode = en_ImageListboxScrollBarMode.eAuto;

            this._defaultItemColor = SystemColors.Control;
            this._selectedItemColor = Color.CornflowerBlue;
            updateItemColor();  //выставить указанный _defaultItemColor во всех итемах

            _itemsList.ev_ListChanged += itemsListChanged;
            base.SizeChanged += controlSizeChanged;

            this.VScrollBar.Scroll += scrollBarScroll;
        }
    }
}
